define( 
	[ 
		"qlik",
		"angular",
		"./DomEdI_UniqLine_Properties",
		"text!./DomEdI_UniqLine_Template.ng.html",
		"css!./DomEdI_UniqLine_Styles.css" 
	],
	function ( 	
		qlik, 
		angular,
		props,
		template
) {
	"use strict";
	var qlik = window.require('qlik');
	return {
		initialProperties: {
			qHyperCubeDef: {
				qDimensions: [],
				qMeasures: [],
				qInitialDataFetch: [{
					qWidth	: 4,
					qHeight	: 2000
				}]
			},
			selectionMode : "CONFIRM"
		},
		definition: props,
		support : {
			snapshot: true,
			export: true,
			exportData : false
		},
		template: template,
		controller: [ '$scope', '$element', function( $scope, $element ){
			function processData() {
				var _result = [];
				$scope.layout.qHyperCube.qDataPages[0].qMatrix.forEach(function(row, key){
					_result.push({
						name	: row[0].qText,
						count	: row[1].qText
					})
				});
				$scope.itemWidth = Math.ceil( ( $element.width() / ( _result.length * 2 ) ) ) - 1 + "px";
				$scope.uniqLineData = _result;
			}

			processData();
			$scope.component.model.Validated.bind(function () {
				processData();
			});
			$scope.$watch(
				function () {
					return [$element[0].offsetWidth, $element[0].offsetHeight].join('x');
				},
				function (value) {
					processData();
				}
			)
		} ],
		paint: function( $element ){
			console.log("Extension DomEdI_UniqLine mounted");
			var _w = $element.find("td.data")[0].offsetWidth;
			var _h = $element.find("td.data")[0].offsetHeight;
			var _wh = _w < _h ? _w : _h;
			$element.find(".dataPoint").each(function(){
				this.style.width = _wh + "px";
				this.style.height = _wh + "px";
			})
		}
	};

} );

